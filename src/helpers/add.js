var Handlebars = require('handlebars');

/**
 * @param {integer} no1
 * @param {integer} no2
 * @example
 * {{add 2, 1}} or:
 * {{add @index, 1}}
 * @returns Summ of two numbers
 */

module.exports = function(no1, no2) {
  return no1+no2;
};
