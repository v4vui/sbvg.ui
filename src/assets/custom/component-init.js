$(document).ready(function () {
    $('.bet-btn').on('click', function () {
        $(this).toggleClass('active');
    });
    initGameNavigation();
    $('.scrollbar-macosx').scrollbar();
});

const initGameNavigation = function () {
    const mainGameNavigation = new Flickity( '.game-select', {
        asNavFor: document.querySelector('.game-container'),
        accessibility: false,
        contain: true,
        pageDots: false,
        groupCells: true,
        cellAlign: 'left',
        selectedAttraction: 0.2,
        friction: 0.8
    });
    const gameRoundNavigation = new Flickity( '.round-select', {
        asNavFor: document.querySelector('.bet-carousel'),
        accessibility: false,
        contain: true,
        pageDots: false,
        groupCells: true,
        cellAlign: 'left',
        selectedAttraction: 0.2,
        friction: 0.8
    });
    const roundBetingSlider = new Flickity( '.bet-carousel', {
        accessibility: false,
        cellAlign: 'left',
        contain: true,
        pageDots: false,
        prevNextButtons: false,
        freeScroll: false,
        fade: true,
        draggable: false
    });
    const roundForecastSlider = new Flickity( '.forecast-carousel', {
        asNavFor: document.querySelector('.bet-carousel'),
        accessibility: false,
        cellAlign: 'left',
        contain: true,
        pageDots: false,
        prevNextButtons: false,
        freeScroll: false,
        fade: true,
        draggable: false
    });
    const allGames = new Flickity( '.game-container', {
        accessibility: false,
        cellAlign: 'left',
        contain: true,
        pageDots: false,
        prevNextButtons: false,
        freeScroll: false,
        fade: true,
        draggable: false,
        selectedAttraction: 0.2,
        friction: 0.8
    });
};
