# SBVG UI based on ZURB WebApp Template

[![devDependency Status](https://david-dm.org/zurb/foundation-zurb-template/dev-status.svg)](https://david-dm.org/zurb/foundation-zurb-template#info=devDependencies)


## Installation

To use this template, your computer needs:

- [NodeJS](https://nodejs.org/en/) (Version 6 or greater recommended, tested with 6.11.4 and 8.12.0)
- [Git](https://git-scm.com/)
- [yarn](https://yarnpkg.com/lang/en/docs/install/)

### Manual Setup

Go to the project root and install the needed dependencies:
```bash
yarn
```

Run `yarn start` to run Gulp and the watcher.
```bash
yarn start
```

The finished site will be created in a folder called `dist`, viewable at this URL:
```
http://localhost:8000
```

To create compressed, production-ready assets, run `yarn run build`.
```bash
yarn run build
```